<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\ConnectionType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class ConnectionController extends Controller
{
    /**
     * @Route("/connection", name="connection")
     */
    public function index(AuthenticationUtils $utils) { 
    
        $error =$utils->getLastAuthenticationError();

        $lastEmail = $utils->getLastUsername();
        
        return $this->render('connection/index.html.twig', [
            'error' => $error,
            'lastEmail' => $lastEmail
        ]);
    }
}
