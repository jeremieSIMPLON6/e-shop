<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ProductRepository;
use App\Entity\Category;

class ProductCategoryController extends Controller
{
    /**
     * @Route("/category/{cat}", name="product_category")
     */
    public function show(Category $cat, ProductRepository $repo)
    {
    
        $products = $repo->findBy(

            array('category' => $cat));

        return $this->render('product_category/index.html.twig', [
            "products" => $products,
            "imageURI" => $this->getParameter('brochures_URI')
        ]);
    }
}
