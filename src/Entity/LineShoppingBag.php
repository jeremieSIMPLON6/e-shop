<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LineShoppingBagRepository")
 */
class LineShoppingBag
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ShoppingCard", inversedBy="lineShoppingBags")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shoppingBag;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="lineShoppingBags")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Product;

    public function getId()
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getShoppingBag(): ?ShoppingCard
    {
        return $this->shoppingBag;
    }

    public function setShoppingBag(?ShoppingCard $shoppingBag): self
    {
        $this->shoppingBag = $shoppingBag;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->Product;
    }

    public function setProduct(?Product $Product): self
    {
        $this->Product = $Product;

        return $this;
    }
}
